![logo](assets/images/logo/logo.png?raw=true)

# VoltaTube

VoltaTube is streaming software similar to [YouTube](https://youtube.com) or [Netflix](https://netflix.com).

## Features
* Uses PDO for MySQL Connection
* User upload
  * Private
  * Public

## License


## Documentation
Some documentation might be found here on the [wiki](../../wikis/home/). Most will be added on [the project's website](https://volta-tube.eu), which will also have a [forum](https://volta-tube.eu/forum).
