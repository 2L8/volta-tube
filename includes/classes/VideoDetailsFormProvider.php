<?php

class VideoDetailsFormProvider
{
    private $con;

    public function __construct($con)
    {
            $this->con = $con;
    }

    public function createUploadForm()
    {
        $fileInput = $this->createFileInput();
        $titleInput = $this->createTitleInput();
        $descriptionInput = $this->createDescriptionInput();
        $privacyInput = $this->createPrivacyInput();
        $categoriesInput = $this->createCategoriesInput();
        $uploadButton = $this->createUploadButton();

        return "<form class='' action='processing.php' method='POST'>
                    $fileInput
                    $titleInput
                    $descriptionInput
                    $categoriesInput
                    $privacyInput
                    $uploadButton
                </form>";
    }

    public function createFileInput()
    {
        return "<div class='mb-3'>
                  <input class='form-control' type='file' id='formFile' name='fileInput' required>
                </div>";
    }

    public function createTitleInput()
    {
        return "<div class='mb-3'>
                    <input class='form-control' type='text' placeholder='Title' name='titleInput'>
                </div>";
    }

    public function createDescriptionInput()
    {
        return "<div class='mb-3'>
                    <textarea class='form-control' id='exampleFormControlTextarea1' placeholder='Description' name='descriptionInput' rows='3'></textarea>
                </div>";
    }

    public function createPrivacyInput()
    {
        return "<div class='mb-3'>
                    <select class='form-select' name='privacyInput'>
                      <option value='0'>Private</option>
                      <option value='1'>Public</option>
                    </select>
                </div>";
    }

    public function createCategoriesInput()
    {
        $query = $this->con->prepare("SELECT * FROM categories");
        $query->execute();

        $html = "<div class='mb-3'>
                    <select class='form-select' name='categoryInput'>";

        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {

            $id = $row["id"];
            $name = $row["name"];

            $html .= "<option value='$id'>$name</option>";
        }

        $html .= "</select>
            </div>";

      return $html;
    }

    private function createUploadButton()
    {
        return "<button type='submit' name='uploadButton' class='btn btn-primary'>Upload</button>";
    }
}

?>
