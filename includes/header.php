<?php require_once("includes/config.php"); ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <!-- FontAwesome CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/master.css">

    <title>VoltaTube</title>
    <!-- favicon -->
    <link rel="shortcut icon" type="image/jpg" href="assets/images/logo/favicon.png"/>
  </head>
  <body>
    <div id="pageContainer"><!-- start page container -->

      <div id="mastHeadContainer"><!-- start navbar container -->
        <button type="button" name="button" class="navShowHide">
          <img src="assets/images/icons/menu.png" />
        </button>

        <a href="index.php" class="logoContainer">
          <img src="assets/images/logo/logo.png" title="logo" alt="Site Logo" />
        </a>

        <div class="searchBarContainer">
          <form class="" action="search.php" method="GET">
            <input type="text" name="term" class="searchBar" value="" placeholder="Search...">
            <button type="button" name="button" class="searchButton">
              <img src="assets/images/icons/search.png" title="search" alt="Search" />
            </button>
          </form>
        </div>

        <div class="rightIcons">
          <a href="upload.php">
            <img src="assets/images/icons/upload.png" class="upload" title="upload" alt="Upload" />
          </a>

          <a href="#">
            <img src="assets/images/profilePictures/default.png" class="upload" title="upload" alt="Upload" />
          </a>
        </div>
      </div><!-- end navbar container -->

      <div id="sideNavContainer" style="display: none;"><!-- start sidebar container -->

      </div><!-- end sidebar container -->

      <div id="mainSectionContainer"><!-- start section container -->

        <div id="mainContentContainer"><!-- start content container -->
